package hafidz.hz.mvvm_recyclerview.data.dao


import com.google.gson.annotations.SerializedName

data class AsmaulHusnaDao(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("status")
    val status: String
)