package hafidz.hz.mvvm_recyclerview.data.dao


import com.google.gson.annotations.SerializedName

data class En(
    @SerializedName("meaning")
    val meaning: String
)