package hafidz.hz.mvvm_recyclerview.data.model

data class AsmaulHusnaModel(
    val arabName: String,
    val englishName: String,
    val number: Int,
    val meaning: String
)