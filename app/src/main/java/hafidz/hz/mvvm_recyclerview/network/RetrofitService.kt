package hafidz.hz.mvvm_recyclerview.network

import hafidz.hz.mvvm_recyclerview.data.dao.AsmaulHusnaDao
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface RetrofitService {
    @GET("/asmaAlHusna")
    fun getAllAsmaulHusna(): Observable<AsmaulHusnaDao>

    @GET("/asmaAlHusna/{id}")
    fun getAsmaulHusna(@Path("id") id: Int): Observable<AsmaulHusnaDao>
}