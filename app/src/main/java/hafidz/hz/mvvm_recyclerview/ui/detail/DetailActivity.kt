package hafidz.hz.mvvm_recyclerview.ui.detail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import hafidz.hz.mvvm_recyclerview.R
import hafidz.hz.mvvm_recyclerview.ui.main.MainViewModel
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    private lateinit var detailViewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        detailViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            DetailViewModel::class.java
        )

        val number = intent.getIntExtra("number", 1)

        detailViewModel.getData(number)
        detailViewModel.asmaulHusna.observe(this, Observer {
            txt_detail.text = it.toString()
        })
    }
}
