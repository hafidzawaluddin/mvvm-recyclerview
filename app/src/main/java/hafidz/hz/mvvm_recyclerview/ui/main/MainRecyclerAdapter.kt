package hafidz.hz.mvvm_recyclerview.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hafidz.hz.mvvm_recyclerview.R
import hafidz.hz.mvvm_recyclerview.data.model.AsmaulHusnaModel
import kotlinx.android.synthetic.main.main_recycler_layout.view.*

class MainRecyclerAdapter(private val models: List<AsmaulHusnaModel>, private val listener:(AsmaulHusnaModel) -> Unit) :
    RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.main_recycler_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = models.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindItem(models[position], listener)


    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bindItem(model: AsmaulHusnaModel, listener: (AsmaulHusnaModel) -> Unit) {
            view.txt_arab.text = model.arabName
            view.txt_english.text = model.englishName
            view.txt_meaning.text = model.meaning
            view.setOnClickListener { listener(model) }
        }
    }
}