package hafidz.hz.mvvm_recyclerview.ui.main

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hafidz.hz.mvvm_recyclerview.data.model.AsmaulHusnaModel
import hafidz.hz.mvvm_recyclerview.network.RetrofitClient
import hafidz.hz.mvvm_recyclerview.network.RetrofitService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainViewModel: ViewModel() {

    private val _asmaulHusna = MutableLiveData<List<AsmaulHusnaModel>>()
    private val retrofitService: RetrofitService = RetrofitClient.getRetrofitService()

    val asmaulHusna: LiveData<List<AsmaulHusnaModel>>
        get() = _asmaulHusna

    @SuppressLint("CheckResult")
    private fun getAsmaulHusna() {
        retrofitService.getAllAsmaulHusna()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val listAsmaulHusna = mutableListOf<AsmaulHusnaModel>()
                it.data.forEach { data ->
                    listAsmaulHusna.add(AsmaulHusnaModel(data.name, data.transliteration, data.number, data.en.meaning))
                }
                _asmaulHusna.postValue(listAsmaulHusna)
            }, {
                it.printStackTrace()
            })
    }

    init {
        getAsmaulHusna()
    }
}