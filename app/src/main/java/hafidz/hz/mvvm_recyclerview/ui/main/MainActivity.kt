package hafidz.hz.mvvm_recyclerview.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import hafidz.hz.mvvm_recyclerview.R
import hafidz.hz.mvvm_recyclerview.data.model.AsmaulHusnaModel
import hafidz.hz.mvvm_recyclerview.ui.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainViewModel: MainViewModel
    private lateinit var mainRecyclerAdapter: MainRecyclerAdapter
    private val asmaulHusnaModels = mutableListOf<AsmaulHusnaModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainRecyclerAdapter = MainRecyclerAdapter(asmaulHusnaModels) {
            val intent = Intent(this, DetailActivity::class.java)
            intent.putExtra("number", it.number)
            startActivity(intent)
        }

        main_recycler.adapter = mainRecyclerAdapter

        mainViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            MainViewModel::class.java
        )

        mainViewModel.asmaulHusna.observe(this, Observer {
            asmaulHusnaModels.addAll(it)
            mainRecyclerAdapter.notifyDataSetChanged()
        })
    }
}
