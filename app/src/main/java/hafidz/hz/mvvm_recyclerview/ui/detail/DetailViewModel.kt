package hafidz.hz.mvvm_recyclerview.ui.detail

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hafidz.hz.mvvm_recyclerview.data.dao.AsmaulHusnaDao
import hafidz.hz.mvvm_recyclerview.data.model.AsmaulHusnaModel
import hafidz.hz.mvvm_recyclerview.network.RetrofitClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailViewModel : ViewModel() {

    private val _asmaulHusna = MutableLiveData<AsmaulHusnaModel>()
    private val retrofitService = RetrofitClient.getRetrofitService()

    val asmaulHusna: LiveData<AsmaulHusnaModel>
        get() = _asmaulHusna

    @SuppressLint("CheckResult")
    fun getData(number: Int){
        retrofitService.getAsmaulHusna(number)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val asmaulHusnaModel = AsmaulHusnaModel(it.data[0].name, it.data[0].transliteration, it.data[0].number, it.data[0].en.meaning)
                _asmaulHusna.postValue(asmaulHusnaModel)
            }, {
                it.printStackTrace()
            })
    }
}